import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import sys

matplotlib.use('Agg')

plt.rcParams["figure.figsize"] = (12,8)

args = sys.argv

data = np.fromfile(args[1], np.single)

logData = 10*np.log10(data)

plt.plot(np.arange(0, len(logData)/1e6-0.5/1e6, 1/1e6),logData)
plt.xlabel("Time [Sec]")
plt.ylabel("Power [dB]")

plt.savefig(args[2], bbox_inches='tight')
