# importing requests and json
import requests, json, datetime
import numpy as np
import matplotlib.pyplot as plt

# base URL
BASE_URL = "https://api.openweathermap.org/data/3.0/onecall?"
LAT = 40.76
LON = -111.84
API_KEY = 2
EXLUDE = "minutely,hourly,daily"
UNITS = "imperial"
# upadting the URL
URL = BASE_URL + "lat=" + str(LAT) + "&lon=" + str(LON) + "&exclude=" + EXLUDE + "&units=" + UNITS + "&appid=" + API_KEY
# HTTP request
response = requests.get(URL)
# checking the status code of the request
if response.status_code == 200:
    # getting data in the json format
    data = response.json()
    temp = data["current"]["temp"]
    wid = data["current"]["weather"][0]["id"]

    f = open("weather.log", "a")
    f.write(datetime.datetime.now().strftime("%Y-%m-%d,%H:%M:%S,") + str(temp) + "," + str(wid) + "\n")
    f.close()

else:
   # showing the error message
   print("Error in the HTTP request")