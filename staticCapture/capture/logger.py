import requests, json, datetime
import numpy as np
import matplotlib.pyplot as plt
import requests
import sys
from bs4 import BeautifulSoup

args = sys.argv

vcp = "error"
URL = "https://www.roc.noaa.gov/wsr88d/Operations/vcp.asp"
response = requests.get(URL)

if response.status_code == 200:
    soup = BeautifulSoup(response.content, "html.parser")
    entries = soup.findAll('td')


    for entry in entries:
        if("KMTX" in str(entry)):
            vcp = str(entries[entries.index(entry)+1]).strip("</td>").split(" ")[-1]

# base URL
BASE_URL = "https://api.openweathermap.org/data/3.0/onecall?"
LAT = 40.76
LON = -111.84
API_KEY = "d5ab536a5bc797fc2fa5ce370dcf6510"
EXLUDE = "minutely,hourly,daily"
UNITS = "imperial"
# upadting the URL
URL = BASE_URL + "lat=" + str(LAT) + "&lon=" + str(LON) + "&exclude=" + EXLUDE + "&units=" + UNITS + "&appid=" + API_KEY
# HTTP request
response = requests.get(URL)
# checking the status code of the request
if response.status_code == 200:
    # getting data in the json format
    data = response.json()
    temp = data["current"]["temp"]
    wid = data["current"]["weather"][0]["id"]

    f = open("/mydata/weather.log", "a")
    f.write(datetime.datetime.now().strftime("%Y-%m-%d,%H:%M:%S,") + str(temp) + "," + str(wid) + "," + vcp + "\n")
    f.close()

else:
   # showing the error message
   print("Error in the HTTP request")

#plt.plot(f, PSD_shifted)
#plt.xlabel("Frequency [MHz]")
#plt.ylabel("Magnitude [dB]")
#plt.grid(True)
#plt.show()