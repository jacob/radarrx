#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

/* 200MHz options
#define MICROSEC_SIZE 200
#define NUM_SAMPLES 120000000000
*/
#define MICROSEC_SIZE 200
#define NUM_SAMPLES 120000000000
#define CHUNK_SIZE 2000
#define SKIP_SIZE 1000000000
//#define IN_FILE "12-06-Short.iq"
//#define OUT_FILE "12-06-Pwr.dat"

struct iq
{
    short i;
    short q;
};

float microSecAvgPwr(int start, struct iq *data){
    float avg = 0.0f;//Running average storage
    float i_value, q_value;//Store float i and q values
    float pwr;//Power variable

    for(int i=start; i<start+MICROSEC_SIZE; i += 1) {//Compute average for 1 microsecond
        i_value = data[i].i;//Get i value
        q_value = data[i].q;//get q value
        pwr = (float)(i_value * i_value + q_value * q_value);//Compute power
        avg += pwr;//Add power to running average
    }
    
    avg = avg/(MICROSEC_SIZE);//Average power over 1 microsecond

    return avg;//Return the average power
}

int main(int argc, char *argv[]) {
    if(argc != 3){
        printf("Incorrect number of arguments. Usage: ./shortAvgPwr <input file> <output file>");
        return 0;
    }

    printf("%ld, %ld", sizeof(short), sizeof(struct iq));

    //Load command line arguments
    char* IN_FILE = argv[1];
    char* OUT_FILE = argv[2];

    int i, j;//Index variables
    FILE *fIn = fopen(argv[1], "r");//Open the file containing short iq data
    FILE *fOut = fopen(argv[2], "w");//Open the file to write the reduced data to

    struct iq *data = malloc(sizeof(struct iq) * CHUNK_SIZE);//Allocate memory for the data chunks

    float *results = malloc(sizeof(float) * (CHUNK_SIZE/MICROSEC_SIZE));//Allocate memory for the results

    int read = 0;//Number of bytes read

    fseek(fIn, SKIP_SIZE * sizeof(struct iq), SEEK_SET);//Skip the first 1 billion samples

    for(i=0; i<NUM_SAMPLES/CHUNK_SIZE; ++i) {//Process chuck of data

        read = fread(data, sizeof(struct iq), CHUNK_SIZE, fIn);//Read a chunk of data into memory

        for(j=0; j<CHUNK_SIZE/MICROSEC_SIZE; ++j) {//Process chuck of data

            results[j] = microSecAvgPwr(j*MICROSEC_SIZE, data);//Get data for 1 usec and store in results

        }

        fwrite(results, sizeof(float), CHUNK_SIZE/MICROSEC_SIZE, fOut);//Write chunk results to file
    }

    return 0;
}
