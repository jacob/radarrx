import emulab_sslxmlrpc.client.api as api
import emulab_sslxmlrpc.xmlrpc as xmlrpc
import json
import time
import schedule
from random import randint

def stillRunning(rpc):
    params = {
    "experiment" : "PowderSandbox,AutoRadarRX",
    "asjson"     : True
    }
    (exitval,response) = api.experimentStatus(rpc, params).apply();
    return(response.value != 12)
    

def runRadarRx():
    
    # Create the RPC server object.
    print("Creating RPC Server")
    config = {
        "debug"       : 0,
        "impotent"    : 0,
        "verify"      : 0,
        "certificate" : ".ssl/emulab.pem",
    }
    rpc = xmlrpc.EmulabXMLRPC(config)

    while stillRunning(rpc):
        print("Waiting for Experiment to Terminate")
        time.sleep(60)

    #Define the experiment I would like to start
    print("Configuring and Starting Experiment")
   
    params = {
        "profile" : "NRDZ,radarRx",
        "proj"    : "PowderSandbox",
        "name"    : "AutoRadarRX",
        "paramset": "bill3,weekauto",
        "duration": "3"#Set duration incase terminate fails to free resources after 3 hours
    }
    
    (exitval,response) = api.startExperiment(rpc, params).apply();

    #Wait for the experiment to boot and Startup to Finish
    print("Waiting for Experiment to Boot")
    params = {
    "experiment" : "PowderSandbox,AutoRadarRX",
    "asjson"     : True
    }

    while True:
        (exitval,response) = api.experimentStatus(rpc, params).apply();
        status = json.loads(response.value)
        if status[u'status'] == "ready" and status[u'execute_status'][u'finished'] == 1:
            break
        if status[u'status'] == "failed":
            break
        print("Waiting..." + status[u'status'])
        time.sleep(30)

    print("Experiment Finished Startup")
    return

    print("Waiting 60Min to terminate")
    time.sleep(3600);

    print("Terminating")
    #Stop the experiment
    params = {
        "experiment" : "PowderSandbox,radarBill3"
    }
    #(exitval,response) = api.terminateExperiment(rpc, params).apply();

    return


def genTimes():
    # Generate 4 random times between 0:00 and 23:59
    times = sorted([random.randint(0, 24*60-1) for _ in range(4)])

    for t in times:
        print(t/60, t%60)

    for t in times:
        t_sec = t * 60
        now = time.time()
        delay = t_sec - (now % (24*60*60))
        if delay >= 0:
            time.sleep(delay)
            runRadarRx()

schedule.every().day.at("00:00").do(genTimes)

while True:
    schedule.run_pending()
    time.sleep(1)