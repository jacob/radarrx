// Client side C/C++ program to demonstrate Socket
// programming
#include <arpa/inet.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>

struct header //Structure to hold information about the data to be transmitted
{
	int id; //The id of the client
	int length; //The length of the data array to be transmitted
    int size; //The number of samples this data represents
    int sampleRate; //The sample rate of the collected data
    int centerFreq; //The center frequency of the collected data
    int gain; //The gain of the collected data
	int magic; //The magic number to make sure the data is correct
};

const int PORT = 8080;
const char* HOST = "localhost";

char *lookup_host (const char *host, char *addrstr)
{
  struct addrinfo hints, *res, *result;
  int errcode;
  void *ptr;

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  errcode = getaddrinfo (host, NULL, &hints, &result);
  if (errcode != 0)
    {
      printf("Issue resolving hostname\n");
    }
  
  res = result;

  while (res)
    {
      inet_ntop (res->ai_family, res->ai_addr->sa_data, addrstr, 100);

      switch (res->ai_family)
        {
        case AF_INET:
          ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
          break;
        case AF_INET6:
          ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
          break;
        }
      inet_ntop (res->ai_family, ptr, addrstr, 100);
      res = res->ai_next;
    }
  
  freeaddrinfo(result);

  return addrstr;
}

int main(int argc, char const* argv[])
{
	int status, valread, client_fd;
	struct sockaddr_in serv_addr;
	struct header captureInfo;
	captureInfo.id = 1;
	captureInfo.length = 1024;
	captureInfo.size = 10240;
	captureInfo.sampleRate = 2000000;
	captureInfo.centerFreq = 100000000;
	captureInfo.gain = 0;
	captureInfo.magic = 1234321;
	char buffer[1024] = { 0 };
	if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		return -1;
	}

	char addrstr[16]; // space to hold the IPv4 string "xxx.xxx.xxx.xxx"
	lookup_host(HOST, addrstr); // Lookup the IP address of the hostname
	printf("Host: %s\n", addrstr);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_port = htons(PORT);

	// Convert IPv4 and IPv6 addresses from text to binary
	// form
	if (inet_pton(AF_INET, addrstr, &serv_addr.sin_addr)
		<= 0) {
		printf(
			"\nInvalid address/ Address not supported \n");
		return -1;
	}

	if ((status = connect(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)))< 0) 
    {
		printf("\nConnection Failed \n");
		return -1;
	}
    
	send(client_fd, &captureInfo, sizeof(struct header), 0);
	printf("Hello message sent\n");
	valread = read(client_fd, buffer, 1024 - 1); // subtract 1 for the null terminator at the end
	printf("%s\n", buffer);

	// closing the connected socket
	close(client_fd);
	return 0;
}
