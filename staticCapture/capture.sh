#!/bin/bash

cd /local/repository/capture

now="$(date +%F_%H-%M)"

echo -e  "Starting Radar Capture"

uhd_rx_cfile -A RX2 --samp-rate 200e6 -f 2800e6 -g 5 -N 121e9 -s /nvme/${now}.cs16

echo -e "Processing Capture"

gcc -o shortpwr shortAvgPwr.c -lm -march=native -mtune=native -ftree-vectorize -mavx512f -O3

./shortpwr /nvme/${now}.cs16 /nvme/${now}.pwr

echo -e "Saving .png"

python3 plotPwr.py /nvme/${now}.pwr /nvme/${now}.png

mkdir /mydata/${now}

cp -r /nvme/* /mydata/${now}/.

cd
