#!/bin/bash
now="$(date +%F_%H-%M)UTCn310"
echo "${now}"
center_freq=2740 # MHz
sample_rate=125 # MHz
gain=0 # dB
group_size=10000 # samples
length=5 # minutes
skip=1000000 # samples
num_samples=$((length * 60 * sample_rate * 1000000 + skip))

mkdir ${now}
echo "Capture Information" >> ${now}/log.txt
echo "Device: " >> ${now}/log.txt
uhd_find_devices >> ${now}/log.txt 2>&1
echo "VCP: $@" >> ${now}/log.txt
echo "Center Frequency: ${center_freq} MHz" >> ${now}/log.txt
echo "Sample Rate: ${sample_rate} MHz" >> ${now}/log.txt
echo "Gain: ${gain} dB" >> ${now}/log.txt
echo "Group Size: ${group_size} samples" >> ${now}/log.txt
echo "Length: ${length} minutes" >> ${now}/log.txt
echo "Skip: ${skip} samples" >> ${now}/log.txt
echo "Number of Samples: ${num_samples} samples" >> ${now}/log.txt
echo "Raw IQ Data, ${now}.cs16, stored as (short)I(short)Q" >> ${now}/log.txt
echo "Power Data, ${now}.pwr, stored as (float)power" >> ${now}/log.txt
echo "FFT Data, avg.fft/max.fft, stored as (double)" >> ${now}/log.txt

echo -e  "n310 Starting Radar Capture"
uhd_rx_cfile --args="serial=3176DFC" -A RX2 --samp-rate ${sample_rate}e6 -f ${center_freq}e6 -g ${gain} -N ${num_samples} -s ${now}/${now}.cs16 >> ${now}/rxLog.txt 2>&1 
echo -e "$(date +%F_%H-%M)"

echo -e "n310 Processing Capture"
gcc -o fft fft.c -lm -lfftw3 -march=native -mtune=native -ftree-vectorize -mavx512f -O3
./fft ${sample_rate} ${skip} ${num_samples} ${group_size} ${now}/${now}.cs16 ${now}/avg${group_size}.fft ${now}/max${group_size}.fft &
wait 

echo -e "n310 Saving Figures"
python3 plotfft.py ${group_size} ${sample_rate} ${center_freq} ${length} ${now}/max${group_size}.fft ${now}/maxFFTPlot.png
feh ${now}/maxFFTPlot.png

echo -e "n310 Finished"
