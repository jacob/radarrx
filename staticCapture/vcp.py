import requests
from bs4 import BeautifulSoup


URL = "https://www.roc.noaa.gov/wsr88d/Operations/vcp.asp"
page = requests.get(URL)

soup = BeautifulSoup(page.content, "html.parser")
entries = soup.findAll('td')

for entry in entries:
    if("KMTX" in str(entry)):
        #print("Found KMTX")
        vcp = str(entries[entries.index(entry)+1]).strip("</td>").split(" ")[-1]
        print(vcp)