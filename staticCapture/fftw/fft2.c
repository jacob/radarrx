#include<complex.h>
#include <math.h>
#include <fftw3.h>
#include <stdio.h>
#include <stdlib.h>

//Define constants
#define CHUNK_SIZE 2048

struct iq //Structure to hold short iq data from SDR
{
    short i;
    short q;
};

//Global Variables
struct iq data[CHUNK_SIZE];//Setup array to hold short iq data
double average[CHUNK_SIZE];//Setup array to hold average results
double maximum[CHUNK_SIZE];//Setup array to hold maximum results
double minimum[CHUNK_SIZE];//Setup array to hold minimum results
fftw_complex in[CHUNK_SIZE]; //Setup fft input array
fftw_complex out[CHUNK_SIZE]; //Setup fft output array
double psd[CHUNK_SIZE];//Setup array for fft results

//Main Code
int main(int argc, char *argv[]) {
    if(argc != 7){
        printf("Incorrect number of arguments. Usage: ./fft <skipSize> <numSamples> <groupSize> <input file> <avg output file> <max output file>");
        return 0;
    }

    //Load command line arguments
    int skipSize = atoi(argv[1]);
    long numSamples = atol(argv[2]);
    int groupSize = atoi(argv[3]);
    int samplesPerGroup = groupSize * CHUNK_SIZE;
    char* IN_FILE = argv[4];
    char* OUT_FILE = argv[5];
    char* OUT_FILE2 = argv[6];

    printf("Skip Size: %d \n", skipSize);
    printf("Number of Samples: %ld \n", numSamples);
    printf("Group Size: %d \n", groupSize);
    printf("Samples Per Group: %d \n", samplesPerGroup);

    int i, j, k;//Index variables
    FILE *fIn = fopen(IN_FILE, "r");//Open the file containing short iq data
    FILE *fOut = fopen(OUT_FILE, "w");//Open the file to write the average psd results to
    FILE *fOut2 = fopen(OUT_FILE2, "w");//Open the file to write the maximum psd results to

    fftw_plan plan = fftw_plan_dft_1d(CHUNK_SIZE, in, out, FFTW_FORWARD, FFTW_MEASURE); //Setup fft plan

    int read = 0; //Number of bytes read

    fseek(fIn, sizeof(struct iq) * skipSize, SEEK_SET); //Skip the first SKIP_SIZE samples

    for(i=0; i < numSamples/samplesPerGroup; ++i) { //Process groups of samples

        for(j = 0; j < groupSize; ++j){
            read = fread(data, sizeof(struct iq), CHUNK_SIZE, fIn); //Read a chunk of data into memory
            printf("Read %d bytes at group %d \n", read, i);

            if(read == CHUNK_SIZE){ //Only calculate fft of chucks with a full CHUNK_SIZE samples
                for(k=0; k<CHUNK_SIZE; ++k) { //Convert data to fftw_complex format
                    in[k] = (double)data[k].i + (double)data[k].q * I;
                }

                fftw_execute(plan); //Execute fft

                for(k=0; k<CHUNK_SIZE; ++k) {
                    //psd[k] = creal(in[k]) * creal(in[k]) + cimag(in[k]) * cimag(in[k]);//Calculate power spectral density
                    psd[k] = out[k] * conj(out[k]);//Calculate power spectral density
                    if(j == 0){ //The first sample of the group is both the average and maximum
                        average[k] = psd[k];//Store average results for the group
                        maximum[k] = psd[k];//Store maximum results for the group
                    }
                    else{
                        average[k] = (average[k] + psd[k]);//Store average results for the group
                        if(psd[k] > maximum[k]){//Store maximum results for the group
                            maximum[k] = psd[k];
                        }
                    }
                }
            }
        }

        for(k=0; k<CHUNK_SIZE; ++k) {
            average[k] = average[k] / groupSize;//Calculate average results for the group
        }

        //Write the second half of the results to the file then the first half to center the data on 0 Hertz 
        fwrite(average + CHUNK_SIZE/2, sizeof(double), CHUNK_SIZE/2, fOut);//Write chunk average results to file
        fwrite(average, sizeof(double), CHUNK_SIZE/2, fOut);//Write chunk average results to file
        fwrite(maximum + CHUNK_SIZE/2, sizeof(double), CHUNK_SIZE/2, fOut2);//Write chunk maximum results to file
        fwrite(maximum, sizeof(double), CHUNK_SIZE/2, fOut2);//Write chunk maximum results to file
    }

    return 0;
}
