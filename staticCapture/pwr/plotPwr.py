import matplotlib
matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt
import subprocess
import sys

matplotlib.use('Agg')

plt.rcParams["figure.figsize"] = (12,8)

args = sys.argv

chunkSize = 2048 # number of samples per chunk
groupSize = int(args[1]) # number of chunks per group
Fs = int(float(args[2])) # sample rate
Ts = 1/(Fs * 1e6) # sample period
print("Sample period: ", Ts, " seconds")
C = int(float(args[3])) # center frequency
M = int(1e6) # megahertz
fileTime = int(args[4])*60 # time of file in seconds

data = np.fromfile(args[5], np.single)
CalculatedTime = len(data) * Fs * Ts
print("Calculated time: ", CalculatedTime, " seconds", " | Actual time: " , fileTime, " seconds")
t = np.arange(0, CalculatedTime, CalculatedTime/len(data))

logData = 10*np.log10(data)

plt.plot(t, logData)
plt.xlabel("Time [Sec]")
plt.ylabel("Power [dB]")

if len(args) > 6:
    plt.savefig(args[6], bbox_inches='tight')
else:
    plt.show()
