#!/bin/bash
now="$(date +%F_%H-%M)"

echo "Starting Radar Capture"
uhd_rx_cfile -A RX2 --samp-rate 50e6 -f 2725e6 -g 5 -N 181e6 -s ${now}.cs16

echo -e "Processing Capture"
gcc -o pwr avgPwr.c -lm -march=native -mtune=native -ftree-vectorize -mavx512f -O3
./pwr ${now}.cs16 ${now}.pwr
python3 plot.py ${now}.pwr ${now}.png