#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>

#define CHUNK_SIZE 2000
#define SKIP_SIZE 100//0000000

struct iq
{
    short i;
    short q;
};

int microSec_Size;

float microSecAvgPwr(int start, struct iq *data){
    float avg = 0.0f;//Running average storage
    float i_value, q_value;//Store float i and q values
    float pwr;//Power variable

    for(int i=start; i<start+microSec_Size; i += 1) {//Compute average for 1 microsecond
        i_value = data[i].i;//Get i value
        q_value = data[i].q;//get q value
        pwr = (float)(i_value * i_value + q_value * q_value);//Compute power
        avg += pwr;//Add power to running average
    }
    
    avg = avg/(microSec_Size);//Average power over 1 microsecond

    return avg;//Return the average power
}

int main(int argc, char *argv[]) {
    if(argc != 3){
        printf("Incorrect number of arguments. Usage: ./shortAvgPwr <input file> <output file>");
        return 0;
    }

    //printf("%ld, %ld", sizeof(short), sizeof(struct iq));

    //Load command line arguments
    microSec_Size = atoi(argv[1]);
    char* IN_FILE = argv[2];
    char* OUT_FILE = argv[3];

    int i, j;//Index variables
    FILE *fIn = fopen(IN_FILE, "r");//Open the file containing short iq data
    FILE *fOut = fopen(OUT_FILE, "w");//Open the file to write the reduced data to

    struct iq *data = malloc(sizeof(struct iq) * CHUNK_SIZE);//Allocate memory for the data chunks

    float *results = malloc(sizeof(float) * (CHUNK_SIZE/microSec_Size));//Allocate memory for the results

    fseek(fIn, SKIP_SIZE * sizeof(struct iq), SEEK_SET);//Skip the first 1 billion samples

    int read = fread(data, sizeof(struct iq), CHUNK_SIZE, fIn);//Read the first chunk of data
    while(read == CHUNK_SIZE){//While there are full chucks to process
        for(j=0; j<CHUNK_SIZE/microSec_Size; ++j) {//Process chuck of data
            results[j] = microSecAvgPwr(j*microSec_Size, data);//Get data for 1 usec and store in results
        }

        fwrite(results, sizeof(float), CHUNK_SIZE/microSec_Size, fOut);//Write chunk results to file
        read = fread(data, sizeof(struct iq), CHUNK_SIZE, fIn);//Read the next chunk of data
    }

    return 0;
}
