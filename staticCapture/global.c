#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <pthread.h> 
#include <complex.h>
#include <string.h>
#include <math.h>
#include <fftw3.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>

#define MAGIC 1234567890
#define ID 1

typedef unsigned int uint;

// Constants
const char* HOST = "localhost";
const int PORT = 8080;
const int FFTSIZE = 2048;
const int GROUPSIZE = 1000;
const int SAMPLERATE = 125000000;
const int CENTERFREQ = 2835000000;
const int GAIN = 0;

// Global variables
fftw_plan plan;
fftw_complex in[2048]; //Setup fft input array
fftw_complex out[2048]; //Setup fft output array
pthread_mutex_t fftwLock; //Mutex lock for fftw
pthread_mutex_t sendLock; //Mutex lock for sending data 

struct iq //Structure to hold short iq data from SDR
{
    short i;
    short q;
};

struct header //Structure to hold information about the data to be transmitted
{
    uint id; //The id of the sender
    uint length; //The length of the data array to be transmitted also the size of the fft
    uint size; //The number of samples this data represents
    uint sampleRate; //The sample rate of the collected data
    uint centerFreq; //The center frequency of the collected data
    uint gain; //The gain of the collected data
    uint magic; //The magic number to make sure the data is correct
};

struct header buildHeader(uint length, uint size)
{
    struct header info;
    info.id = getpid();
    info.length = length;
    info.size = size;
    info.sampleRate = SAMPLERATE;
    info.centerFreq = CENTERFREQ;
    info.gain = GAIN;
    info.magic = MAGIC;
    return info;
}

char *lookupHost (const char *host, char *addrstr)
{
  struct addrinfo hints, *res, *result;
  int errcode;
  void *ptr;

  memset (&hints, 0, sizeof (hints));
  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags |= AI_CANONNAME;

  errcode = getaddrinfo (host, NULL, &hints, &result);
  if (errcode != 0)
    {
      printf("Issue resolving hostname\n");
    }
  
  res = result;

  while (res)
    {
      inet_ntop (res->ai_family, res->ai_addr->sa_data, addrstr, 100);

      switch (res->ai_family)
        {
        case AF_INET:
          ptr = &((struct sockaddr_in *) res->ai_addr)->sin_addr;
          break;
        case AF_INET6:
          ptr = &((struct sockaddr_in6 *) res->ai_addr)->sin6_addr;
          break;
        }
      inet_ntop (res->ai_family, ptr, addrstr, 100);
      res = res->ai_next;
    }
  
  freeaddrinfo(result);

  return addrstr;
}

void sendResults(double *data)
{
  int status, client_fd;
	struct sockaddr_in serv_addr;
	struct header captureInfo = buildHeader(sizeof(fftw_complex) * FFTSIZE, FFTSIZE);
	pthread_mutex_lock(&sendLock); //Lock mutex with send access

  if ((client_fd = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		printf("\n Socket creation error \n");
		pthread_exit(0);
	}

	char addrstr[16]; // space to hold the IPv4 string "xxx.xxx.xxx.xxx"
	lookupHost(HOST, addrstr); // Lookup the IP address of the hostname
	printf("Host: %s\n", addrstr); // Report the IP address back to the user
	serv_addr.sin_family = AF_INET; // Use IPv4
	serv_addr.sin_port = htons(PORT); // Set the port to connect to

	// Convert IPv4 and IPv6 addresses from text to binary
	if (inet_pton(AF_INET, addrstr, &serv_addr.sin_addr) <= 0) {
		printf("\nInvalid address/ Address not supported \n");
		pthread_exit(0);
	}

	if ((status = connect(client_fd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)))< 0) 
  {
    printf("\nConnection Failed \n");
		pthread_exit(0);
	}
    
	send(client_fd, &captureInfo, sizeof(struct header), 0); // Send the header to the server
	printf("Header sent\n");
  send(client_fd, data, sizeof(double) * FFTSIZE, 0); // Send the data to the server
  printf("Data sent\n");

	// closing the connected socket
	close(client_fd);
  pthread_mutex_unlock(&sendLock); //Unlock mutex with send access
}

void *calcfft(void *vargp) 
{ 
  struct iq *data = (struct iq *)vargp; //Cast void pointer to struct iq pointer
  double psd[FFTSIZE]; //Setup array for fft results
  double maxPSD[FFTSIZE]; //Setup array for max fft results from group results

  pthread_mutex_lock(&fftwLock); //Lock mutex with fftw access
  for(int i = 0; i < GROUPSIZE; i++){ //Calculate the fft of every FFTSIZE samples
    for(int j = 0; j < FFTSIZE; j++){ //Copy data from struct iq array to fftw_complex array
      in[j] = (double)data[i * FFTSIZE + j].i + (double)data[i * FFTSIZE + j].q * I; //Convert to complex double
    }

    fftw_execute(plan); //Execute fft

    for(int j = 0; j < FFTSIZE; j++){ //Calculate power spectral density
      psd[j] = (pow(abs(out[j]), 2) / SAMPLERATE*FFTSIZE);

      if(i == 0){ //If this is the first fft, set the max psd to the current psd
        maxPSD[j] = psd[j];
      }
      else if (psd[j] > maxPSD[j]){ //If the current psd is greater than the max psd, replace it
        maxPSD[j] = psd[j];
      }
    }
  }
  pthread_mutex_unlock(&fftwLock); //Unlock mutex with fftw access

  free(data); //Free memory allocated for data
  sendResults(maxPSD); //Send results to client
  
  pthread_exit(0); //Exit thread
}


void *collectData(FILE *fIn)
{
  sleep((FFTSIZE*GROUPSIZE)/SAMPLERATE); //Mimic delay from waiting for actual radio
  void *data = malloc(sizeof(struct iq) * FFTSIZE * GROUPSIZE); //Allocate memory for the data
  int read = fread(data, sizeof(struct iq), FFTSIZE * GROUPSIZE, fIn); //Read a chunk of data into memory
  if(read != FFTSIZE * GROUPSIZE){ //If the read was not the correct size, exit
    printf("Read was not correct size\n");
    sleep(600);
    exit(-1);
  }
  return data;
}

int main() 
{ 
  if (pthread_mutex_init(&fftwLock, NULL) != 0) { 
        printf("\n mutex init has failed\n"); 
        exit(-1); 
  }

  if (pthread_mutex_init(&sendLock, NULL) != 0) { 
        printf("\n mutex init has failed\n"); 
        exit(-1); 
  }

  plan = fftw_plan_dft_1d(FFTSIZE, in, out, FFTW_FORWARD, FFTW_MEASURE); //Setup fft plan

  FILE *fIn = fopen("../test1.cs16", "r");//Open the file containing short iq data

  for(int c = 0; c < 10000; c++){
    pthread_t thread_id; 
    printf("Collecting Data %d \n", c);
    void *data = collectData(fIn); //Collect data from file
    pthread_create(&thread_id, NULL, calcfft, data); //Create thread to calculate and send fft
    pthread_detach(thread_id);
    printf("After Thread\n");  
  }

  sleep(600);
}
