sudo apt-get update -y
sudo apt-get install -y qt5-default libfftw3-dev cmake pkg-config libliquid-dev python3-tk libuhd-dev uhd-host
sudo apt-get install -y build-essential git
git clone https://github.com/miek/inspectrum.git
cd inspectrum/
mkdir build
cd build/
cmake ..
make -j4
sudo make install

apt install -y python3-pip
pip3 install numpy
pip3 install matplotlib
cd

mkfs -t ext4 /dev/nvme0n1
mkdir -p nvme1
mount -t auto /dev/nvme0n1 nvme1
chmod 777 nvme1

mkfs -t ext4 /dev/nvme2n1
mkdir -p nvme2
mount -t auto /dev/nvme2n1 nvme2
chmod 777 nvme2

mkfs -t ext4 /dev/nvme3n1
mkdir -p nvme3
mount -t auto /dev/nvme3n1 nvme3
chmod 777 nvme3

mkfs -t ext4 /dev/nvme1n1
mkdir -p nvme4
mount -t auto /dev/nvme1n1 nvme4
chmod 777 nvme4

lsblk -f
