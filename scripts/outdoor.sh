#!/bin/bash
now="$(date +%F_%H-%M)"
center_freq=2700 # MHz
sample_rate=100 # MHz
gain=0 # dB
group_size=10 # samples
length=1 # minutes
skip=1000000000 # samples
num_samples=$((length * 60 * sample_rate * 1000000 + skip))

mkdir ${now}
echo "Capture Information" >> ${now}/log.txt
echo "Device: " >> ${now}/log.txt
uhd_find_devices >> ${now}/log.txt 2>&1
echo "VCP: $@" >> ${now}/log.txt
echo "Center Frequency: ${center_freq} MHz" >> ${now}/log.txt
echo "Sample Rate: ${sample_rate} MHz" >> ${now}/log.txt
echo "Gain: ${gain} dB" >> ${now}/log.txt
echo "Group Size: ${group_size} samples" >> ${now}/log.txt
echo "Length: ${length} minutes" >> ${now}/log.txt
echo "Skip: ${skip} samples" >> ${now}/log.txt
echo "Number of Samples: ${num_samples} samples" >> ${now}/log.txt
echo "Raw IQ Data, ${now}.cs16, stored as (short)I(short)Q" >> ${now}/log.txt
echo "Power Data, ${now}.pwr, stored as (float)power" >> ${now}/log.txt
echo "FFT Data, avg.fft/max.fft, stored as (double)" >> ${now}/log.txt

echo -e  "Starting Radar Capture"
uhd_rx_cfile -A RX2 --samp-rate ${sample_rate}e6 -f ${center_freq}e6 -g ${gain} -N ${num_samples} -s ${now}/${now}.cs16

echo -e "Processing Capture"
gcc -o ppwr pwr/avgPwr.c -lm -march=native -mtune=native -ftree-vectorize -mavx512f -O3
gcc -o fft fftw/fft.c -lm -lfftw3 -march=native -mtune=native -ftree-vectorize -mavx512f -O3
./ppwr ${sample_rate} ${skip} ${now}/${now}.cs16 ${now}/${now}.pwr &
./fft ${skip} ${num_samples} ${group_size} ${now}/${now}.cs16 ${now}/avg.fft ${now}/max.fft &
wait 

echo -e "Saving Figures"
python3 pwr/plotPwr.py ${now}/${now}.pwr ${now}/pwrPlot.png &
python3 fftw/plotfft.py ${group_size} ${sample_rate} ${center_freq} ${length} ${now}/avg.fft ${now}/avgFFTPlot.png &
python3 fftw/plotfft.py ${group_size} ${sample_rate} ${center_freq} ${length} ${now}/max.fft ${now}/maxFFTPlot.png &
wait

echo -e "Finished"
