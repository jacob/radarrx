#!/bin/bash
now="onemin"
center_freq=2780 # MHz
sample_rate=200 # MHz
gain=0 # dB
group_size=10000 # samples
length=1 # minutes
skip=1000000000 # samples
num_samples=$((length * 60 * sample_rate * 1000000 + skip))
num_groups=$((num_samples / (group_size*2048)))
echo -e "Starting Capture of ${num_groups} groups of ${group_size} samples"


echo -e "Processing Capture of ${num_samples} samples"
#gcc -o ppwr pwr/avgPwr.c -lm -march=native -mtune=native -ftree-vectorize -mavx512f -O3
gcc -o fft fftw/fft2.c -lm -lfftw3 -march=native -mtune=native -ftree-vectorize -mavx512f -O3
#./ppwr ${sample_rate} ${skip} test/${now}.cs16 test/${now}.pwr &
./fft ${skip} ${num_samples} ${group_size} test/${now}.cs16 test/avg.fft test/max.fft &
wait 

echo -e "Saving Figures"
#python3 pwr/plotPwr.py ${group_size} ${sample_rate} ${center_freq} ${length} test/${now}.pwr test/pwrPlot.png &
python3 pwr/plotfft.py ${group_size} ${sample_rate} ${center_freq} ${length} test/avg.fft #test/avgFFTPlot.png &
python3 fftw/plotfft.py ${group_size} ${sample_rate} ${center_freq} ${length} test/max.fft #test/maxFFTPlot.png &
wait

echo -e "Finished"
