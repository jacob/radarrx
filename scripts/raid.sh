sudo apt-get update -y
sudo apt-get install -y qt5-default libfftw3-dev cmake pkg-config libliquid-dev python3-tk libuhd-dev uhd-host mdadm firefox
sudo apt-get install -y build-essential git
git clone https://github.com/miek/inspectrum.git
cd inspectrum/
mkdir build
cd build/
cmake ..
make -j4
sudo make install

sudo sysctl -w net.core.wmem_max=24862979

apt install -y python3-pip
pip3 install numpy
pip3 install matplotlib
cd

sudo mdadm --create --verbose /dev/md/nvme --level=0 --raid-devices=4 /dev/nvme0n1 /dev/nvme1n1 /dev/nvme2n1 /dev/nvme3n1
sudo mkfs.ext4 -F /dev/md/nvme
sudo mkdir nvme
sudo mount /dev/md/nvme nvme/
sudo chmod 777 nvme

sudo fstrim nvme/

v=$(netstat -ie | grep -B1 "192.168.40.1 " | head -n 1 | awk '{print $1}')

interface=${v%:}

ip link set mtu 9000 dev ${interface}

