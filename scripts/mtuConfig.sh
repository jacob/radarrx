#!/bin/bash
v=$(netstat -ie | grep -B1 "192.168.40.1 " | head -n 1 | awk '{print $1}')

interface=${v%:}

ip link set mtu 9000 dev ${interface}
