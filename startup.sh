#sudo add-apt-repository -y ppa:ettusresearch/uhd
#sudo add-apt-repository -y ppa:johnsond-u/sdr
sudo apt-get update
sudo apt-get install python3-pip

sudo apt install -y qt5-default libfftw3-dev cmake pkg-config libliquid-dev python3-tk libuhd-dev uhd-host mdadm feh build-essential git inspectrum gnuradio htop uhd-host

sudo pip3 install beautifulsoup4

sudo sysctl -w net.core.rmem_max=24862979
sudo sysctl -w net.core.wmem_max=24862979

sudo mdadm --create --verbose /dev/md/nvme --level=0 --raid-devices=4 /dev/nvme0n1 /dev/nvme1n1 /dev/nvme2n1 /dev/nvme3n1
sudo mkfs.ext4 -F /dev/md/nvme
sudo mkdir /nvme
sudo mount /dev/md/nvme /nvme/
sudo chmod 777 /nvme

sudo fstrim /nvme/

v=$(netstat -ie | grep -B1 "192.168.40.1 " | head -n 1 | awk '{print $1}')

interface=${v%:}

sudo ip link set mtu 9000 dev ${interface} 

sudo rm /usr/bin/uhd_rx_cfile
chmod +x /local/repository/capture/uhd_rx_cfile
sudo cp /local/repository/capture/uhd_rx_cfile /usr/bin/

cd /local/repository/capture

now="$(date +%F_%H-%M)"

echo -e  "Starting Radar Capture"

currentDate=`date`
echo $currentDate

uhd_rx_cfile -A RX2 --samp-rate 200e6 -f 2800e6 -g 5 -N 121e9 -s /nvme/${now}.cs16

sudo python3 /local/repository/capture/logger.py

echo -e "Processing Capture"

gcc -o shortpwr shortAvgPwr.c -lm -march=native -mtune=native -ftree-vectorize -mavx512f -O3

./shortpwr /nvme/${now}.cs16 /nvme/${now}.pwr

echo -e "Saving .png"

python3 plotPwr.py /nvme/${now}.pwr /nvme/${now}.png
echo "results" | mail -s "Radar Results ${now}" jacob.bills@utah.edu -A /nvme/${now}.png

echo -e "Moving data to longterm storage"
sudo mkdir /mydata/${now}

sudo cp -r /nvme/* /mydata/${now}/.

currentDate=`date`
echo $currentDate

sudo ed /etc/sysctl.conf << "EDEND"
a
net.core.rmem_max=24862979
net.core.wmem_max=24862979
.
w
EDEND

#sudo "/usr/lib/uhd/utils/uhd_images_downloader.py -t x310"
