# first of all import the socket library
import socket
import time
import threading
import numpy as np
import array
import matplotlib.pyplot as plt
#from scipy.signal import find_peaks
import sys
import csv
import argparse

def getData(s):
    c, addr = s.accept() #Accept a connection from a client	 

    buf = c.recv(28) #Receive a 28 byte header

    try: #Attempt to get metadata from header
        header = np.frombuffer(buf, dtype=np.uint32)
    except: #If the header is bad return None
        print("Bad Header")
        c.close()
        return True, None, None
        
    if header[-1] != 1234567890: #If the magic number has been corrupted in some way return None
        print("Bad magic number")
        c.close()
        return True, None, None

    size = header[1] # size of the incoming data
    #print("id =", header[0], "size =", size, i)
    buf = c.recv(size)
    try: #Attempt to load the data
        data = np.frombuffer(buf, dtype=np.single)
    except: #If something is wrong with the data return None
        print("Bad Data")
        c.close()
        return True, None, None

    c.close() #Close the connection
    
    data = 10*np.log10(np.abs(data + 0.00000000000001))
    return False, header, np.fft.fftshift(data)
    
def channelOccupancy(data, centerFreq, bandwidth, ch1, ch2, threshold=-40, v=False):
    # data is a 2D array of spectrum power levels in dBX
    # centerFreq is the center frequency of the spectrum in Hz
    # bandwidth is the bandwidth of the spectrum in Hz
    # prominence is the prominence parameter for scipy.signal.find_peaks
    # width is the width parameter for scipy.signal.find_peaks
    # returns a 1D array of occupancy data from 2700 MHz to 2900 MHz
    #print("Checking", centerFreq, bandwidth, ch1, ch2, threshold)
    now = time.time()
    occupiedCh1 = False
    occupiedCh2 = False
    freq = np.arange((centerFreq-(bandwidth/2)), (centerFreq+(bandwidth/2)), bandwidth/len(data))
    for i in range(len(data)):
        #print(data[i])
        if data[i] >= threshold:
            #print(data[i], freq[i])
            if freq[i] >= ch1-2e6 and freq[i] <= ch1+2e6:
                occupiedCh1 = True
            if freq[i] >= ch2-2e6 and freq[i] <= ch2+2e6:
                occupiedCh2 = True

    if occupiedCh1 and v: print("Channel 1(", ch1, "Hz) occupied at", now)
    if occupiedCh2 and v: print("Channel 2(", ch2, "Hz) occupied at", now)

    return (occupiedCh1, occupiedCh2)

def calibrateOccThreshold(s, ch1, ch2, threshold, timeout, cuts, verbose, bottomOut=-100):
    # data is a 2D array of spectrum power levels in dBX
    # centerFreq is the center frequency of the spectrum in Hz
    # bandwidth is the bandwidth of the spectrum in Hz
    # ch1 is the center frequency of the first channel in Hz
    # ch2 is the center frequency of the second channel in Hz
    # threshold is the initial threshold to start with

    now = time.time()
    count = 0
    print("Looking for", cuts, "cuts in", timeout, "seconds")
    print("Testing threshold at", threshold, "dBX/Hz")
    while True:
        bad, header, data = getData(s)
        if not bad: #If getData returned None there was an error parsing the data then move on
            occupiedCh1, occupiedCh2 = channelOccupancy(data, header[4], header[3], ch1, ch2, threshold, v=verbose) #Determine if the channels are occupied
            if occupiedCh1 and occupiedCh2: # both channels are occupied raise the threshold
                threshold += 2
                now = time.time()
            elif occupiedCh1 ^ occupiedCh2: # Only one channel is occupied we have a good signal
                count += 1 #Note another signal has been detected
                if verbose: print("Signal Detected", count)
                if count > cuts: # If we have detected the correct number of cuts we have a good threshold
                    print("Threshold calibrated at", threshold, "dBX/Hz\n")
                    return threshold #Return the calculated threshold
            elif time.time() - now > timeout: #If an insufficent number of cuts have been detected before timeout lower the threshold
                if threshold < bottomOut: #If we bottom out the threshold human intervention is required
                    print("Calibration Failed Check Output")
                    exit()
                threshold -= 5
                now = time.time()
                count = 0
                print("Testing threshold at", threshold, "dBX/Hz")

def coreLoop(occupancyEvent, lock, s, radars, verbose):
    while True:
        bad, header, data = getData(s)
        if not bad:
            for rad in radars:
                ch0, ch1 = channelOccupancy(data, header[4], header[3], rad.channel0, rad.channel1, threshold=rad.threshold, v=verbose)
                if ch0:
                    with lock:
                        if channels[rad.channel0].occupied != ch0: #If the occupancy status has changed and it has changed to occupied
                            occupancyEvent.set() #Set the occupancy event to trigger the update thread
                        channels[rad.channel0].occupied = ch0 #Update the occupancy status
                        channels[rad.channel0].lastOccupied = time.time() #Update the last occupied time
                if ch1:
                    with lock:
                        if channels[rad.channel1].occupied != ch1: #If the occupancy status has changed and it has changed to occupied
                            occupancyEvent.set() #Set the occupancy event to trigger the update thread
                        channels[rad.channel1].occupied = ch1
                        channels[rad.channel1].lastOccupied = time.time()

def updateLoop(occupancyEvent, lock, channels, waitTime):
    while True:
        occupancyEvent.wait(timeout=waitTime) #Wait for the occupancy event to be set or timeout
        with lock:
            for key in channels: #Iterate through all the channels and update their occupancy status
                if channels[key].timeout < time.time() - channels[key].lastOccupied: #If the timeout has been exceeded consider the channel unoccupied
                    channels[key].occupied = False
                print(key, channels[key].occupied, end=" ")
        print()
        

def main():
    class radar:
        def __init__(self, channel0, channel1, scanTime, cuts):
            self.channel0 = channel0
            self.channel1 = channel1
            self.scanTime = scanTime
            self.cuts = cuts
            self.threshold = None
    
    class channel:
        def __init__(self, freq, timeout):
            self.freq = freq
            self.timeout = timeout
            self.occupied = True
            self.lastOccupied = 0

    #Handle command line arguments
    parser = argparse.ArgumentParser(
                    prog='python3 server.py',
                    description='Detects precense of active radar channels between 2700MHz and 2900MHz',
                    epilog='Jacob Bills Thesis Project 2024')

    parser.add_argument("-t", "--threshold", type=int, default=0, help="Starting threshold value for calibration procedure(default: 0)")
    parser.add_argument("-p", "--port", type=int, default=8080, help="Port number for server to open for incoming connections(default: 8080)")
    parser.add_argument("-s", "--settings", type=str, default="settings.csv", help="File containing list of radar systems to be monitored(Default: settings.csv)")
    parser.add_argument("-v", "--verbose", default=False, action="store_true", help="Prints additional output to standard out (default: No)")
    args = parser.parse_args()

    # Set up threading locks and events
    channelLock = threading.Lock()
    occupancyEvent = threading.Event()

    # Initialize the radar systems and channels
    radars = []
    channels = {}
    with open(args.settings, newline='') as csvfile:
        settings = csv.reader(csvfile, delimiter=' ', quotechar='|')
        next(settings, None) # skip the headers
        for row in settings:
            row = row[0].split(',')
            radars.append(radar(float(row[0]), float(row[1]), int(row[2]), int(row[3])))
            channels[float(row[0])] = channel(float(row[0]), int(row[2]))
            channels[float(row[1])] = channel(float(row[1]), int(row[2]))

    print("\nImported", len(channels), "channels from settings file", channels.keys())
    print("\nImported", len(radars), "radars from settings file")        
    for rad in radars:
        print("Radar with channels", int(rad.channel0//1e6), int(rad.channel1//1e6), "and scan time of", rad.scanTime, "containing", rad.cuts, "cuts")

    # next create a socket to take incoming connections
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)		
    s.bind(('', args.port))		 
    s.listen(5)
    print("\nOpened socket on port", args.port)
    
    # Calibrate the threshold for each radar system
    for rad in radars:
        print("\nCalibrating threshold for", rad.channel0//1e6, rad.channel1//1e6, "MHz")
        rad.threshold = calibrateOccThreshold(s, rad.channel0, rad.channel1, args.threshold, rad.scanTime, rad.cuts, args.verbose)

    # Start the core and update threads
    threading.Thread(target=updateLoop, args=(occupancyEvent, channelLock, channels, 60)).start() #Start the update thread
    coreLoop(occupancyEvent, channelLock, s, radars, args.verbose) #Start the core loop


if __name__ == "__main__":
    main()